"""
WSGI config for educa project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import site
import sys

from django.core.wsgi import get_wsgi_application

site.addsitedir('/home/ubuntu/pyvenv/lib/python2.7/site-packages')

sys.path.append('/home/ubuntu/picadx_django/educa')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "educa.settings")

application = get_wsgi_application()


