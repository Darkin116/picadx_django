from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from courses.models import Course, Module
from django.core.management import BaseCommand

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        
        picadx_group, created = Group.objects.get_or_create(name='picadx_users')
        
        add_course = Permission.objects.get(codename='add_course')
        change_course = Permission.objects.get(codename='change_course')
        delete_course = Permission.objects.get(codename='delete_course')
        add_image = Permission.objects.get(codename='add_image')
        change_image = Permission.objects.get(codename='change_image')
        delete_image = Permission.objects.get(codename='delete_image')
        add_file = Permission.objects.get(codename='add_file')
        change_file = Permission.objects.get(codename='change_file')
        delete_file = Permission.objects.get(codename='delete_file')        
        add_module = Permission.objects.get(codename='add_module')
        change_module = Permission.objects.get(codename='change_module')
        delete_module = Permission.objects.get(codename='delete_module')



        for perm in [add_course, change_course, delete_course,add_image, change_image, delete_image, add_file, change_file, delete_file, add_module, change_module, delete_module]:
            if perm not in picadx_group.permissions.all():
                picadx_group.permissions.add(perm) 

        

