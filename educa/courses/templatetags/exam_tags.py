from django import template
import json
import itertools

register = template.Library()

@register.filter
def is_user_answer_checked(sitting, answer):
    u_ans_json = json.loads(sitting.user_answers)
    u_ans_ids = map(int, itertools.chain(*u_ans_json.values()))
    print u_ans_ids
    return answer.id in u_ans_ids
