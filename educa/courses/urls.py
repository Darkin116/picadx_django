from django.conf.urls import url, include
from . import views
from django.conf.urls.static import static
from django.conf import settings
from .admin import admin_site

urlpatterns = [
url(r'^myadmin/', admin_site.urls),
    url(r'^mine/$', views.ManageCourseListView.as_view(), name='manage_course_list'),
    url(r'^create/$', views.CourseCreateView.as_view(), name='course_create'),
    url(r'^(?P<pk>\d+)/edit/$', views.CourseUpdateView.as_view(), name='course_edit'),
    url(r'^(?P<pk>\d+)/delete/$', views.CourseDeleteView.as_view(), name='course_delete'),
    url(r'^module/(?P<module_id>\d+)/content/(?P<model_name>\w+)/create/$',
            views.ContentCreateUpdateView.as_view(), name='module_content_create'),
    url(r'^module/(?P<module_id>\d+)/content/(?P<model_name>\w+)/(?P<id>\d+)/$',
            views.ContentCreateUpdateView.as_view(), name='module_content_update'),
    url(r'^content/(?P<id>\d+)/delete/$', views.ContentDeleteView.as_view(), name='module_content_delete'),

#not being used
url(r'^(?P<course_id>[0-9]+)/quiz/', include('quiz.urls')),

url(r'^(?P<course_id>[0-9]+)/exam/(?P<exam_id>\w+)/take', views.ExamTake.as_view(), name='Take_exam'),

    url(r'^(?P<pk>[0-9]+)/module/(?P<module_id>\d+)/delete/$', views.ModuleDeleteView.as_view(), name='delete_module'),
    url(r'^(?P<pk>[0-9]+)/mission/(?P<module_id>\d+)/delete/$', views.MissionDeleteView.as_view(), name='delete_mission'),

    url(r'^module/(?P<module_id>\d+)/$', views.ModuleContentListView.as_view(), name='module_content_list'),
#create new url for editing materials but use the same template, add X image for removal

    url(r'^(?P<pk>\d+)/module/(?P<module_id>\d+)/$', views.CourseModuleUpdateView.as_view(), name='course_module_update'),
    url(r'^(?P<pk>\d+)/mission/(?P<module_id>\d+)/$', views.CourseMissionUpdateView.as_view(), name='course_mission_update'),

    url(r'^(?P<course_id>[0-9]+)/module/create/$', views.ModuleCreateView.as_view(), name='course_module_create'),
    url(r'^(?P<course_id>[0-9]+)/mission/create/$', views.MissionCreateView.as_view(), name='course_mission_create'),
    url(r'^module/order/$', views.ModuleOrderView.as_view(), name='module_order'),
    url(r'^module/(?P<id>[0-9]+)/followlink/$', views.ModuleFollowLink.as_view(), name='follow_link_material'),
    url(r'^mission/(?P<id>[0-9]+)/followlink/$', views.MissionFollowLink.as_view(), name='follow_link_mission'),
    url(r'^content/order/$', views.ContentOrderView.as_view(), name='content_order'),
    url(r'^$', views.CourseListView.as_view(), name='course_list'),
    url(r'^subject/(?P<subject>[\w-]+)/$', views.CourseListView.as_view(), name='course_list_subject'),
    url(r'^(?P<slug>[\w-]+)/$', views.CourseDetailView.as_view(), name='course_detail'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
