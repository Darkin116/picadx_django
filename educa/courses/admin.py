from django.contrib import admin
from .models import Subject, Course, Module, Mission, Exam, MCAQuestion
from multichoice.models import MCQuestion, Answer
from quiz.models import Quiz, Question
from quiz.admin import MCQuestionAdmin, QuizAdmin, QuizAdminForm
from django.contrib.admin import AdminSite



@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug']
    prepopulated_fields = {'slug': ('title',)}


class ModuleInline(admin.StackedInline):
    model = Module

class MissionInline(admin.StackedInline):
    model = Mission

@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ['title', 'subject', 'created']
    list_filter = ['created', 'subject']
    search_fields = ['title', 'overview']
    prepopulated_fields = {'slug': ('title',)}
    inlines = [ModuleInline, MissionInline]

class PMCQuestionAdmin(MCQuestionAdmin):
    fields = ('content', 
              'figure', 'quiz', 'explanation', 'answer_order')


class MyAdminSite(AdminSite):
    site_header = 'Python administration'

class ExamAdminForm(QuizAdminForm):
    class Meta:
        model = Exam
        exclude = []

class ExamAdmin(QuizAdmin):
    form = ExamAdminForm

admin_site = MyAdminSite(name='myadmin')
admin_site.register(MCAQuestion, PMCQuestionAdmin)
admin_site.register(Exam, ExamAdmin)



