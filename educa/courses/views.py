from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, BaseUpdateView, BaseCreateView
from django.views.generic.detail import DetailView
from django.forms.models import modelform_factory
from django.db.models import Count
from django.apps import apps
from django.core.cache import cache
from braces.views import LoginRequiredMixin, PermissionRequiredMixin, \
                         CsrfExemptMixin, JsonRequestResponseMixin
from students.forms import CourseEnrollForm
from .models import Subject, Course, Module, Content, UserModule, Mission, UserMission, Exam
from .forms import ModuleFormSet, CourseUpdateForm, ModuleUpdateForm, UnpublishedCourseUpdateForm, MissionUpdateForm, ExamForm
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from collections import defaultdict
from django.shortcuts import render
from quiz.views import QuizTake
from quiz.models import Sitting, Question, Progress
from django.db.models.signals import post_save
from django.contrib.auth.models import User, Group

def add_user_picadx(sender, instance, signal, *args, **kwargs):
    if sender is User:
        picadx_group = Group.objects.get(name='picadx_users')
        picadx_group.user_set.add(instance)
post_save.connect(add_user_picadx, sender=User)


class OwnerMixin(object):
    def get_queryset(self):
        qs = super(OwnerMixin, self).get_queryset()
        return qs.filter(owner=self.request.user)


class OwnerEditMixin(object):
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(OwnerEditMixin, self).form_valid(form)


class OwnerCourseMixin(OwnerMixin, LoginRequiredMixin):
    model = Course


class OwnerCourseEditMixin(OwnerCourseMixin, OwnerEditMixin):
    fields = ['badge','title','slug', 'overview']
    success_url = reverse_lazy('manage_course_list')
    template_name = 'courses/manage/course/form.html'

class ManageCourseListView(OwnerCourseMixin, ListView):
    template_name = 'courses/manage/course/list.html'

#add def get, create new course in db, redirect
class CourseCreateView(PermissionRequiredMixin,
                       OwnerCourseEditMixin,
                       CreateView):
    permission_required = 'courses.add_course'
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        #add row to db for course
        course_instance = Course.objects.create(title='', overview='', slug=None, owner=request.user)
        #redirect to course_edit
        return redirect('course_edit', course_instance.id)



#override post, check for empty title, overview, material, validation error if empty, if all tests pass, course.visibility =1
class CourseUpdateView(PermissionRequiredMixin,
                       OwnerCourseEditMixin,
                       UpdateView):
    permission_required = 'courses.change_course'
    def get_success_url(self):
        return reverse('course_list') + '?show_courses_by_owner=1' 
    
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        subject_id = request.POST['subject']
        new_subject = request.POST['createnewprogram']
        qdict = request.POST.copy()
        #if the subject is not in the database, add it
        if subject_id == '-1' and Subject.objects.filter(title=new_subject).count() == 0:
            fresh_subject = Subject(title=new_subject,
                                    slug=new_subject)
            fresh_subject.save()
            qdict['subject'] = fresh_subject.pk
        if 'addmaterial' in request.POST or 'addmission' in qdict:
            course = Course.objects.get(pk=kwargs['pk'])
            form = UnpublishedCourseUpdateForm(qdict, instance=course)
            form.instance.pk = kwargs['pk']
            if form.is_valid():
                form.instance.badge = request.FILES.get('badge')
                form.instance.save(update_fields=form.fields)
                if 'addmaterial' in request.POST:
                    return redirect('course_module_create', form.instance.id)
                else:
                    return redirect('course_mission_create', form.instance.id)
            else:
                print form.errors
        return super(CourseUpdateView, self).post(request, *args, **kwargs)

    def get_form_class(self):
        return CourseUpdateForm
    
    def form_valid(self, form):
        self.object.visibility = True
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


#created new view for adding material
class ModuleCreateView(CreateView):
    model = Module
    template_name = 'courses/manage/module/form.html'
    form_class = ModuleUpdateForm
    
    #@method_decorator(login_required)
    def get_context_data(self, **kwargs):
        context = super(ModuleCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Add Material'
        return context
    def get_success_url(self):
        return reverse('course_edit', kwargs={'pk':self.course.id})
    def dispatch(self, request, *args, **kwargs):
        self.course = get_object_or_404(Course, pk=kwargs['course_id'])
        return super(ModuleCreateView, self).dispatch(request, *args, **kwargs)
    def form_valid(self, form):
        form.instance.course = self.course
        return super(ModuleCreateView, self).form_valid(form)

#view for adding mission
class MissionCreateView(ModuleCreateView):
    model = Mission
    template_name = 'courses/manage/module/form.html'
    form_class = MissionUpdateForm
    def get_context_data(self, **kwargs):
        context = super(MissionCreateView, self).get_context_data(**kwargs)
        context['title'] = 'Add Mission'
        return context

#not in use
class CourseDeleteView(PermissionRequiredMixin,
                       OwnerCourseMixin,
                       DeleteView):
    success_url = reverse_lazy('manage_course_list')
    template_name = 'courses/manage/course/delete.html'
    permission_required = 'courses.delete_course'


class ModuleDeleteView(PermissionRequiredMixin,
                       OwnerCourseMixin,
                       DeleteView):
    model_class = Module
    success_url = reverse_lazy('course_edit')
    template_name = 'courses/manage/course/form.html'
    permission_required = 'courses.delete_module'

    def get(self, request, pk, module_id):
        module = get_object_or_404(self.model_class, id=module_id)
        module.delete()
        return redirect('course_edit', pk)

class MissionDeleteView(ModuleDeleteView):
    model_class = Mission
    success_url = reverse_lazy('course_edit')
    template_name = 'courses/manage/course/form.html'
    permission_required = 'courses.delete_mission'




class CourseModuleUpdateView(TemplateResponseMixin, View):
    template_name = 'courses/manage/module/form_editmaterial.html'
    course = None
    
    #@method_decorator(login_required)

    def get_form_class(self):
        return ModuleUpdateForm

    def dispatch(self, request, pk, module_id):
        self.course = get_object_or_404(Course, id=pk, owner=request.user)
        self.module = get_object_or_404(Module, id=module_id)
        return super(CourseModuleUpdateView, self).dispatch(request, pk, module_id)

    def get(self, request, pk, module_id):
        module = get_object_or_404(Module, id=module_id)
        form = ModuleUpdateForm(instance=module)
        return render(request, self.template_name, {'form': form})

    def post(self, request, pk, module_id):
        module = get_object_or_404(Module, id=module_id)
        form = ModuleUpdateForm(request.POST,instance=module)
        form.instance.pk = module_id
        if form.is_valid():
            form.instance.save(update_fields=form.fields)
            return redirect('course_edit', self.course.id)
        return self.render_to_response({'course': self.course,
                                        'form': form})

class CourseMissionUpdateView(CourseModuleUpdateView):
    template_name = 'courses/manage/module/form_editmission.html'
    course = None
    def get_form_class(self):
        return MissionUpdateForm
    def dispatch(self, request, pk, module_id):
        self.module = get_object_or_404(Mission, id=module_id)
    def get(self, request, pk, module_id):
        module = get_object_or_404(Mission, id=module_id)
        form = MissionUpdateForm(instance=module)
    def post(self, request, pk, module_id):
        module = get_object_or_404(Mission, id=module_id)
        form = MissionUpdateForm(request.POST,instance=module)


class ContentCreateUpdateView(TemplateResponseMixin, View):
    module = None
    model = None
    obj = None
    template_name = 'courses/manage/content/form.html'

    def get_model(self, model_name):
        if model_name in ['text', 'video', 'image', 'file']:
            return apps.get_model(app_label='courses', model_name=model_name)
        return None

    def get_form(self, model, *args, **kwargs):
        Form = modelform_factory(model,
                                 exclude=['owner', 'order', 'created', 'updated'])
        return Form(*args, **kwargs)

    def dispatch(self, request, module_id, model_name, id=None):
        self.module = get_object_or_404(Module,
                                        id=module_id,
                                        course__owner=request.user)
        self.model = self.get_model(model_name)
        if id:
            self.obj = get_object_or_404(self.model,
                                         id=id,
                                         owner=request.user)
        return super(ContentCreateUpdateView,
                     self).dispatch(request, module_id, model_name, id)

    def get(self, request, module_id, model_name, id=None):
        form = self.get_form(self.model, instance=self.obj)
        return self.render_to_response({'form': form,
                                        'object': self.obj})

    def post(self, request, module_id, model_name, id=None):
        form = self.get_form(self.model,
                             instance=self.obj,
                             data=request.POST,
                             files=request.FILES)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
            if not id:
                # new content
                Content.objects.create(module=self.module,
                                       item=obj)
            return redirect('module_content_list', self.module.id)

        return self.render_to_response({'form': form,
                                        'object': self.obj})


class ContentDeleteView(View):

    def post(self, request, id):
        content = get_object_or_404(Content,
                                    id=id,
                                    module__course__owner=request.user)
        module = content.module
        content.item.delete()
        content.delete()
        return redirect('module_content_list', module.id)


class ModuleContentListView(TemplateResponseMixin, View):
    template_name = 'courses/manage/module/content_list.html'

    def get(self, request, module_id):
        module = get_object_or_404(Module,
                                   id=module_id,
                                   course__owner=request.user)

        return self.render_to_response({'module': module})


class ModuleOrderView(CsrfExemptMixin, JsonRequestResponseMixin, View):

    def post(self, request):
        for id, order in self.request_json.items():
            Module.objects.filter(id=id,
                                  course__owner=request.user).update(order=order)
        return self.render_json_response({'saved': 'OK'})


class ContentOrderView(CsrfExemptMixin, JsonRequestResponseMixin, View):

    def post(self, request):
        for id, order in self.request_json.items():
            Content.objects.filter(id=id,
                                   module__course__owner=request.user).update(order=order)
        return self.render_json_response({'saved': 'OK'})

#use login_required for testing on production server
class CourseListView(TemplateResponseMixin, View):
    model = Course
    template_name = 'courses/course/list.html'
    def get(self, request):
        show_courses_by_owner= request.GET.get('show_courses_by_owner')
        subjects = Subject.objects.annotate(total_courses=Count('courses'))
        all_courses = Course.objects.annotate(total_modules=Count('modules')).order_by('subject','title')
        if request.user.is_authenticated():
            if show_courses_by_owner:
                all_courses = all_courses.filter(owner=request.user)
            else:
                all_courses = all_courses.exclude(owner=request.user).filter(visibility=True)
        else:
            all_courses = all_courses.all()
        courses = all_courses
        #calculate course progress
        for course in courses:
            if request.user.is_authenticated():
                links_count = UserModule.objects.filter(user=request.user,
                                                    module__in=course.modules.all()).count()
                if links_count == 0:
                    course.module_progress = 0
                else:
                    course.module_progress = 100 * links_count / course.total_modules

        
        return self.render_to_response({'subjects': subjects,
                                        'courses': courses,
                                        'show_courses_by_owner': show_courses_by_owner})


class CourseDetailView(DetailView):
    model = Course
    template_name = 'courses/course/detail.html'
#def get, if owner, redirect to edit course
    def get_context_data(self, user=None, **kwargs):
        context = super(CourseDetailView, self).get_context_data(**kwargs)
        context['enroll_form'] = CourseEnrollForm(initial={'course':self.object})
        modules = []
        for module in context['object'].modules.all():
            module.viewed = UserModule.objects.filter(user=user, module=module)
            modules.append(module)
        context['modules'] = modules
        exams = self.get_object().exams.all()
        for exam in exams:
            exam.latest_sitting = exam.latest_sitting_for_user(user)
           
        print exams[0].latest_sitting.user_answers
    
        context['exams'] = exams
        return context
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object, user=request.user)
        if self.object.owner==self.request.user:
            return redirect('course_edit', self.object.id)
        return self.render_to_response(context)

class ModuleFollowLink(View):
    def get(self, request, id):
        module = get_object_or_404(Module, id=id)
        #record that user clicked the link unless he already has clicked it
        UserModule.objects.get_or_create(user=request.user,
                                         module=module)
        return redirect(module.description)

class MissionFollowLink(ModuleFollowLink):
    def get(self, request, id):
        module = get_object_or_404(Mission, id=id)
        UserMission.objects.get_or_create(user=request.user,
                                         module=module)
        return redirect(module.description)

#class CreateExamView():

class ExamTake(QuizTake):
    form_class = ExamForm
    template_name="courses/exam/Take_exam.html"
    result_template_name = 'courses/exam/result.html'    
    previous = None

    def dispatch(self, request, *args, **kwargs):
        self.exam = get_object_or_404(Exam, pk=self.kwargs['exam_id'])
        
        if self.exam.draft and not request.user.has_perm('quiz.change_quiz'):
            raise PermissionDenied

        try:
            self.logged_in_user = self.request.user.is_authenticated()
        except TypeError:
            self.logged_in_user = self.request.user.is_authenticated

        if self.logged_in_user:
            self.sitting = Sitting.objects.user_sitting(request.user,
                                                        self.exam)
        else:
            self.sitting = self.anon_load_sitting()

        if self.sitting is False: ##FIXME- when does the single complete template kick in?
            return render(request, self.single_complete_template_name)

        return super(QuizTake, self).dispatch(request, *args, **kwargs)

    #bypass QuizTake get_form
    def get_form(self, *args, **kwargs):
        return self.form_class(**self.get_form_kwargs())

    def get_form_kwargs(self):
        kwargs = super(QuizTake, self).get_form_kwargs()
        kwargs['exam'] = self.exam
        kwargs['questions'] = Question.objects.filter(pk__in=self.sitting._question_ids()).select_subclasses()
        return kwargs

    #bypass QuizTake addition of self.question
    def get_context_data(self, **kwargs):
        context = super(QuizTake, self).get_context_data(**kwargs)
        context['sitting'] = self.sitting
        context['exam'] = self.exam
        context['questions'] = Question.objects.filter(pk__in=self.sitting._question_ids()).select_subclasses()
        return context

    def form_valid_user(self, form):
        progress, c = Progress.objects.get_or_create(user=self.request.user)
        #grab all answers from form
        user_answers = form.submitted_answers()
        #review all answers, check if correct
        #for loop, scoring logic        
        for ques, ans in user_answers.iteritems():
            is_correct = ques.check_if_correct(ans)
            if is_correct is True:
                self.sitting.add_to_score(1)
                progress.update_score(ques, 1, 1)
            else:
                self.sitting.add_incorrect_question(ques)
                progress.update_score(ques, 0, 1)
            self.sitting.remove_first_question()
            self.sitting.add_user_answer(ques, ans)
        print self.sitting.user_answers
        self.sitting.save()
        
    def final_result_user(self):
        self.quiz = self.exam
        return super(ExamTake, self).final_result_user()
        


