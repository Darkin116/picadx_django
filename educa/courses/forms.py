from django import forms
from django.forms.models import inlineformset_factory,  ModelForm, ModelChoiceField
from .models import Course, Module, Mission, Subject
from django.core.exceptions import ObjectDoesNotExist

ModuleFormSet = inlineformset_factory(Course,
                                      Module,
                                      fields=['title', 'description'],
                                      extra=0,
                                      can_delete=True)

class CourseUpdateForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['subject','badge','title','slug','overview']
        labels = {'subject': 'Program'}

    def clean(self):
        cleaned = super(CourseUpdateForm, self).clean()
        if not cleaned.get('subject'):
            raise forms.ValidationError('Class must have a program.')
        if not cleaned.get('badge'):
            raise forms.ValidationError('Class must have a badge.')
        if not cleaned.get('title'):
            raise forms.ValidationError('Class cannot have an empty title.')
        if not cleaned.get('overview'):
            raise forms.ValidationError('Class cannot have an empty overview.')
        if not cleaned.get('slug'):
            raise forms.ValidationError('Class cannot have an empty slug.')
        if self.instance.modules.count() < 1:
            raise forms.ValidationError('Class must have at least 1 material.')


class UnpublishedCourseUpdateForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['subject','badge','title','slug','overview']
        exclude = ['owner', 'visibility','created']
        labels = {'subject': 'Program'}

    subject = ModelChoiceField(queryset=Subject.objects.all())

class ModuleUpdateForm(forms.ModelForm):

    class Meta:
        model = Module
        fields = ['title', 'description']
        labels = {'description': 'URL'}

class MissionUpdateForm(ModuleUpdateForm):

    class Meta:
        model = Mission
        fields = ['title', 'description']
        labels = {'description': 'URL'}
  
class ExamForm(forms.Form):
    def __init__(self, exam, questions, *args, **kwargs):
        self.exam = exam
        self.questions = questions
        super(ExamForm, self).__init__(*args, **kwargs)  
        for q in questions:
            choice_list = [x for x in q.get_answers_list()]              
            self.fields[str(q.id)] = forms.MultipleChoiceField(choices=choice_list)
        
    def submitted_answers(self):
        answers = {}  
        for q in self.questions:
            ans_choices = [] 
            print "q.id", self.cleaned_data, q.id
            for answerid in self.cleaned_data.get(str(q.id)):
                #answer = Answer.objects.get(pk=answerid)
                ans_choices.append(answerid)
            answers[q] = ans_choices    
        return answers
             
        
