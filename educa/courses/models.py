from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.auth.models import User
from .fields import OrderField
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from quiz.models import Quiz, Sitting
from multichoice.models import MCQuestion


class MCAQuestion(MCQuestion):
    def check_if_correct(self, guesses):
        correct_list=[]
        for a in self.get_answers():
            if a.correct:
                correct_list.append(a.id)
        guesses = map(int, guesses)
        print set(correct_list), set(guesses)
        if set(correct_list) == set(guesses):
            return True
        else:
            return False
    
#use this for Program
class Subject(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True, null=True, blank=True)

    class Meta:
        ordering = ('title',)

    def __unicode__(self):
        return self.title

#Class model, overview is 'details'
class Course(models.Model):
    owner = models.ForeignKey(User, related_name='courses_created')
    subject = models.ForeignKey(Subject, related_name='courses', null=True, blank=True)
    title = models.CharField(max_length=200, blank=True)
    slug = models.SlugField(max_length=200, unique=True, blank=True, null=True)
    overview = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    badge = models.ImageField(blank=True,upload_to="badges/")
    visibility= models.BooleanField(default=False)
    students = models.ManyToManyField(User,
                                      related_name='courses_enrolled',
                                      blank=True)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return self.title


class Exam(Quiz):
    course = models.ForeignKey(Course, related_name="exams")

    def latest_sitting_for_user(self, user):
        sittings = Sitting.objects.filter(user=user, quiz=self, complete=True).order_by('-end')
        if sittings:
            return sittings[0]

#material, URL instead of description
class Module(models.Model):
    course = models.ForeignKey(Course, related_name='modules')
    title = models.CharField(max_length=200)
    description = models.URLField()
    order = OrderField(blank=True, for_fields=['course'])
    image = models.ImageField(upload_to="images/", null=True, blank=True)

    class Meta:
        ordering = ['order']
        verbose_name_plural = "modules"

    def __str__(self):
        return '{}. {}'.format(self.order, self.title)

class Mission(models.Model):
    course = models.ForeignKey(Course, related_name='missions')
    title = models.CharField(max_length=200)
    description = models.URLField()
    order = OrderField(blank=True, for_fields=['course'])
    image = models.ImageField(upload_to="images/", null=True, blank=True)

    class Meta:
        ordering = ['order']
        verbose_name_plural = "missions"

    def __str__(self):
        return '{}. {}'.format(self.order, self.title)
    
#currently do not need for picadx
class Content(models.Model):
    module = models.ForeignKey(Module, related_name='contents')
    order = OrderField(blank=True, for_fields=['module'])
    content_type = models.ForeignKey(ContentType,
                                     limit_choices_to={'model__in':('text',
                                                                    'video',
                                                                    'image',
                                                                    'file')})
    object_id = models.PositiveIntegerField()
    item = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ['order']

#tracks when a user clicks a material's link
class UserModule(models.Model):
    user = models.ForeignKey(User, null=False)
    module = models.ForeignKey(Module, null=False)

    class Meta:
        unique_together = ('user', 'module')

class UserMission(models.Model):
    user = models.ForeignKey(User, null=False)
    module = models.ForeignKey(Mission, null=False)

    class Meta:
        unique_together = ('user', 'module')

class ItemBase(models.Model):
    owner = models.ForeignKey(User, related_name='%(class)s_related')
    title = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title

    def render(self):
        return render_to_string('courses/content/{}.html'.format(self._meta.model_name), {'item': self})


class Text(ItemBase):
    content = models.TextField()


class File(ItemBase):
    file = models.FileField(upload_to='files')


class Image(ItemBase):
    file = models.FileField(upload_to='images')


class Video(ItemBase):
    url = models.URLField()
