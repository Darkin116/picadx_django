set -ex
dropdb picadx
createdb picadx
python manage.py migrate
python manage.py loaddata courses/fixtures/*.json
